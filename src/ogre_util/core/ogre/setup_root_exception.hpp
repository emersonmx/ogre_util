/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of ogre_util.

  ogre_util is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ogre_util is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ogre_util.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OGRE_UTIL_CORE_OGRE_SETUP_ROOT_EXCEPTION_HPP_
#define OGRE_UTIL_CORE_OGRE_SETUP_ROOT_EXCEPTION_HPP_

#include <ogre_util/core/exception.hpp>

namespace ogre_util {
namespace core {
namespace ogre {

class SetupRootException : public ogre_util::core::Exception {
    public:
        SetupRootException(const std::string& message="")
                : ogre_util::core::Exception("SetupRootException: "
                        "Could not instantiate a Ogre::Root object.\n\t" +
                        message) {}
};

} /* namespace ogre */
} /* namespace core */
} /* namespace ogre_util */
#endif /* OGRE_UTIL_CORE_OGRE_SETUP_ROOT_EXCEPTION_HPP_ */

