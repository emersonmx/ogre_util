/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of ogre_util.

  ogre_util is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ogre_util is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ogre_util.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>

#include <OgreRoot.h>
#include <OgreRenderWindow.h>
#include <OgreWindowEventUtilities.h>

#include <mxgame/application/application.hpp>
#include <mxgame/util/util.hpp>

#include <ogre_util/core/ogre/setup_root_exception.hpp>
#include <ogre_util/core/ogre/setup_resources_exception.hpp>
#include <ogre_util/core/ogre/setup_render_system_exception.hpp>
#include <ogre_util/core/ogre/setup_render_window_exception.hpp>
#include <ogre_util/core/ogre/initialize_resources_exception.hpp>
#include <ogre_util/core/ogre/setup_scene_exception.hpp>

using namespace std;

class ApplicationTest
        : public mxgame::application::Application,
          public Ogre::WindowEventListener {

    public:
        virtual ~ApplicationTest() {}

    protected:
        virtual void Initialize() {
            SetupOgreRoot();
            SetupOgreResources();
            SetupOgreRenderSystem();
            SetupOgreRenderWindow();
            InitializeOgreResources();
            SetupOgreScene();
        }

        virtual void SetupOgreRoot() {
            try {
                root_ = new Ogre::Root();
            } catch (...) {
                throw ogre_util::core::ogre::SetupRootException(
                    "Couldn't create Ogre::Root object.");
            }
        }

        virtual void SetupOgreResources() {
            try {
                (void) NULL; // Just to test the inclusion of exceptions.
            } catch (...) {
                throw ogre_util::core::ogre::SetupResourcesException(
                    "Couldn't setup Ogre resources.");
            }
        }

        virtual void SetupOgreRenderSystem() {
            if (!root_->restoreConfig()) {
                if (!root_->showConfigDialog()) {
                    throw ogre_util::core::ogre::SetupRenderSystemException(
                        "The setup was canceled.");
                }
            }
        }

        virtual void SetupOgreRenderWindow() {
            window_ = root_->initialise(true, "ApplicationTest");
            if (window_ == NULL) {
                throw ogre_util::core::ogre::SetupRenderWindowException(
                    "Window was not created.");
            }

            Ogre::WindowEventUtilities::addWindowEventListener(window_, this);
        }

        virtual void InitializeOgreResources() {
            try {
                (void) NULL; // Just to test the inclusion of exceptions.
            } catch (...) {
                throw ogre_util::core::ogre::InitializeResourcesException(
                    "Resources are not started.");
            }
        }

        virtual void SetupOgreScene() {
            try {
                scene_manager_ = root_->createSceneManager(Ogre::ST_GENERIC);

                Ogre::Camera* camera = scene_manager_->createCamera("camera");
                Ogre::Viewport* viewport = window_->addViewport(camera);
                viewport->setBackgroundColour(Ogre::ColourValue(0, 0, 0));
                camera->setAspectRatio(Ogre::Real(window_->getWidth()) /
                               Ogre::Real(window_->getHeight()));
            } catch (...) {
                throw ogre_util::core::ogre::SetupSceneException(
                    "Couldn't create Ogre scene.");
            }
        }

        virtual void ProcessEvents() {
            Ogre::WindowEventUtilities::messagePump();
        }

        virtual void Update() {
            ProcessEvents();
        }

        virtual void Render() {
            root_->renderOneFrame();
        }

        virtual void Finalize() throw() {
            DestroyOgre();
        }

        virtual void DestroyOgre() throw() {
            Ogre::WindowEventUtilities::removeWindowEventListener(window_,
                                                                  this);

            delete root_;
        }

        virtual void windowResized(Ogre::RenderWindow* render_window) {}

        virtual void windowClosed(Ogre::RenderWindow* render_window) {
            Exit();
        }

    private:
        Ogre::Root* root_;
        Ogre::RenderWindow* window_;
        Ogre::SceneManager* scene_manager_;
};

MXGAME_UTIL_MAIN(new ApplicationTest())

